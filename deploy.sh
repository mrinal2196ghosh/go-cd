echo "------------- copying artifact to the container ------------"
scp ../deploy_dir.zip vagrant@192.168.33.10:/home/vagrant/artifacts

echo "------------- unzipping artifact in container -------------"
echo "
cd artifacts
rm -rf deploy_dir
unzip ./deploy_dir.zip
cd deploy_dir
python -m SimpleHTTPServer 8000 &
" | ssh vagrant@192.168.33.10 /bin/bash
